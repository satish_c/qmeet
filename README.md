# qMeet

qMeet is a web application that can create short lived meeting rooms on the fly. Each meeting room will have a unique url and people can join these meeting rooms by simply opening the url and choosing a user name. Moderators are responsible for creating meeting rooms and they can moderate the conversation in a meeting room.

This project is a work-in-progress.

## Technology stack
* JavaScript libraries - [React](https://facebook.github.io/react/), [RxJS](https://github.com/Reactive-Extensions/RxJS)
* The JavaScript build and packaging system - [Gulp](http://gulpjs.com/), [Babel](https://babeljs.io/), [Browserify](http://browserify.org/), [npm](https://www.npmjs.com/)
* Java libraries - Spring MVC, Spring Data JPA with Hibernate, Spring Security, Thymeleaf, Jersey
* Java build system - Gradle with npm integration
* Databases - Postgresql, H2 in-memory database.