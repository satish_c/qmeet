CREATE TYPE member_role AS ENUM ('admin', 'moderator');

CREATE TABLE IF NOT EXISTS member (
  id            SERIAL NOT NULL PRIMARY KEY,
  user_name     VARCHAR(255) NOT NULL CONSTRAINT username_unique UNIQUE,
  role          member_role,
  password_hash VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS config (
  id            SERIAL NOT NULL PRIMARY KEY,
  key           VARCHAR(255) NOT NULL CONSTRAINT cfg_key_unique UNIQUE,
  value         VARCHAR(255) NOT NULL
);