package org.qmeet.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
public class Member {
    private int id;
    private String userName;
    private MemberRole memberRole;
    private String passwordHash;

    public Member() {}

    public Member(String userName, String passwordHash, MemberRole memberRole) {
        this.userName = userName;
        this.passwordHash = passwordHash;
        this.memberRole = memberRole;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "role")
    @Type(type = "org.qmeet.model.MemberRoleEnumType")
    public MemberRole getMemberRole() {
        return memberRole;
    }

    public void setMemberRole(MemberRole memberRole) {
        this.memberRole = memberRole;
    }

    @Basic
    @Column(name = "password_hash")
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Member member = (Member) o;

        if (id != member.id) return false;
        if (userName != null ? !userName.equals(member.userName) : member.userName != null) return false;
        if (memberRole != null ? !memberRole.equals(member.memberRole) : member.memberRole != null) return false;
        if (passwordHash != null ? !passwordHash.equals(member.passwordHash) : member.passwordHash != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (memberRole != null ? memberRole.hashCode() : 0);
        result = 31 * result + (passwordHash != null ? passwordHash.hashCode() : 0);
        return result;
    }
}
