package org.qmeet.model;

public enum MemberRole {
    ADMIN("admin"),
    MODERATOR("moderator");

    private String value;

    private MemberRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
