package org.qmeet.rest;

import org.qmeet.bean.ConfigKeys;
import org.qmeet.bean.QmeetProperties;
import org.qmeet.dao.MemberDao;
import org.qmeet.dao.ConfigDao;
import org.qmeet.model.Member;
import org.qmeet.model.Config;
import org.qmeet.model.MemberRole;
import org.qmeet.rest.bean.LoginBean;
import org.qmeet.security.PasswordHash;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("firsttime")
public class FirstTime {
    @Autowired
    private ConfigDao configDao;
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private QmeetProperties qmeetProperties;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Doh";
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public LoginBean register(@Valid LoginBean loginBean) {
        if(!qmeetProperties.isFirstTime()) {
            throw new IllegalAccessError();
        }
        Member member = null;
        try {
            member = new Member(loginBean.getUsername(),
                    PasswordHash.createHash(loginBean.getPassword()),
                    MemberRole.ADMIN);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed user creation");
        }

        memberDao.save(member);

        Config config = new Config(ConfigKeys.FIRST_TIME.getStringValue(), "false");
        configDao.save(config);
        qmeetProperties.setFirstTime(false);

        return loginBean;
    }
}