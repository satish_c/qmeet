package org.qmeet.rest;

import org.hibernate.validator.constraints.NotEmpty;
import org.qmeet.dao.MeetingRoomDao;
import org.qmeet.model.Member;
import org.qmeet.model.MemberRole;
import org.qmeet.rest.bean.LoginBean;
import org.qmeet.rest.bean.Message;
import org.qmeet.rest.bean.RoomBean;
import org.qmeet.rest.exception.RestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Path("meeting-room")
public class MeetingRoom {
    @Autowired
    private MeetingRoomDao meetingRoomDao;

    @GET
    @Path("/{roomId}")
    @Produces(MediaType.APPLICATION_JSON)
    public RoomBean getRoom(@NotEmpty @PathParam("roomId") String roomId) {
        org.qmeet.model.MeetingRoom room = meetingRoomDao.findByRoomId(roomId);
        RoomBean roomBean = new RoomBean();
        roomBean.setName(room.getName());
        roomBean.setExpiresIn(Duration.between(LocalDateTime.now(), room.getValidTill().toLocalDateTime()).toMinutes());
        return roomBean;
    }
}