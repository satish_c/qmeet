package org.qmeet.rest.exception;

import org.qmeet.rest.bean.CommonBean;

public class RestException extends Exception {
    private CommonBean bean;

    public RestException(CommonBean bean) {
        super();
        this.bean = bean;
    }

    public CommonBean getBean() {
        return bean;
    }
}
