package org.qmeet.rest.exception;

import org.glassfish.jersey.server.validation.ValidationError;
import org.qmeet.rest.bean.CommonBean;
import org.qmeet.rest.bean.Message;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {
    private static final Logger LOGGER = Logger.getLogger(ValidationExceptionMapper.class.getName());
    @Override
    public Response toResponse(ValidationException exception) {
        LOGGER.log(Level.FINE, "Validation error", exception);
        if (exception instanceof ConstraintViolationException) {
            final ConstraintViolationException cve = (ConstraintViolationException) exception;
            final Set<ConstraintViolation<?>> constraintViolations = cve.getConstraintViolations();
            final CommonBean commonBean = new CommonBean();
            for(final ConstraintViolation<?> constraintViolation : constraintViolations) {
                final ValidationError validationError = new ValidationError();
                commonBean.addMessage(new Message(Message.ERROR, constraintViolation.getMessage()));
            }
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(commonBean)
                    .build();
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(exception.getMessage())
                .build();
    }
}

