package org.qmeet.rest.bean;

public class Message {
    private String type;
    private String text;
    public final static String INFO = "info";
    public final static String WARNING = "warning";
    public final static String ERROR = "danger";

    public Message() {
        super();
    }

    public Message(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
