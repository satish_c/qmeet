package org.qmeet.rest.bean;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class RoomBean extends CommonBean {
    @NotEmpty(message = "Meeting room name is required")
    private String name;

    @NotNull(message = "Duration is required")
    @Min(15)
    @Max(120)
    private long durationInMin;

    private long expiresIn;
    private int memberCount;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDurationInMin() {
        return durationInMin;
    }

    public void setDurationInMin(long durationInMin) {
        this.durationInMin = durationInMin;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}