package org.qmeet.rest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotEmpty;
import org.qmeet.dao.MeetingRoomDao;
import org.qmeet.dao.MemberDao;
import org.qmeet.model.MeetingRoom;
import org.qmeet.model.Member;
import org.qmeet.model.MemberRole;
import org.qmeet.rest.exception.RestException;
import org.qmeet.rest.bean.CommonBean;
import org.qmeet.rest.bean.LoginBean;
import org.qmeet.rest.bean.Message;
import org.qmeet.rest.bean.RoomBean;
import org.qmeet.security.PasswordHash;
import org.qmeet.util.UrlGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Path("moderator")
public class Moderator {
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private MeetingRoomDao meetingRoomDao;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public LoginBean register(@Valid LoginBean loginBean) throws RestException {
        Member member = memberDao.findByUserName(loginBean.getUsername());
        if(member != null) {
            loginBean.addMessage(new Message(Message.ERROR, "Username already exists"));
            throw new RestException(loginBean);
        }

        try {
            member = new Member(loginBean.getUsername(),
                    PasswordHash.createHash(loginBean.getPassword()),
                    MemberRole.MODERATOR);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed user creation");
        }

        memberDao.save(member);
        return loginBean;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<LoginBean> getModerators() {
        List<Member> members = memberDao.findByMemberRole(MemberRole.MODERATOR);
        List<LoginBean> loginBeans = new ArrayList<>();
        for(Member member : members) {
            LoginBean loginBean = new LoginBean();
            loginBean.setUsername(member.getUserName());
            loginBeans.add(loginBean);
        }
        return loginBeans;
    }

    @DELETE
    @Path("/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public CommonBean deleteModerator(@NotEmpty @PathParam("username") String userName) {
        memberDao.deleteByUserName(userName);
        CommonBean commonBean = new CommonBean();
        commonBean.addMessage(new Message(Message.INFO, "Deleted moderator: " + userName));
        return commonBean;
    }

    @DELETE
    @Path("/moderators")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public CommonBean deleteModerators(@NotNull List<String> userNames) {
        for(String userName : userNames) {
            memberDao.deleteByUserName(userName);
        }
        CommonBean commonBean = new CommonBean();
        commonBean.addMessage(new Message(Message.INFO, "Deleted " + userNames.size() + " moderators"));
        return commonBean;
    }

    @GET
    @Path("/meeting-room")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RoomBean> getMeetingRooms() {
        Iterable<org.qmeet.model.MeetingRoom> meetingRooms = meetingRoomDao.findAll();
        List<RoomBean> rooms = new ArrayList<>();
        for(org.qmeet.model.MeetingRoom meetingRoom : meetingRooms) {
            RoomBean roomBean = new RoomBean();
            roomBean.setName(meetingRoom.getName());
            roomBean.setExpiresIn(Duration.between(LocalDateTime.now(), meetingRoom.getValidTill().toLocalDateTime()).toMinutes());
            roomBean.setUrl(meetingRoom.getUrl());
            rooms.add(roomBean);
        }
        return rooms;
    }

    @POST
    @Path("/meeting-room")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional("h2TransactionManager")
    public RoomBean createMeetingRoom(@Valid RoomBean roomBean) throws RestException {
        org.qmeet.model.MeetingRoom meetingRoom = new org.qmeet.model.MeetingRoom();
        meetingRoom.setName(roomBean.getName());
        meetingRoom.setValidTill(Timestamp.valueOf(LocalDateTime.now().plusMinutes(roomBean.getDurationInMin())));
        meetingRoom.setUrl(UrlGenerator.getUniqueString());
        meetingRoomDao.save(meetingRoom);
        roomBean.addMessage(new Message(Message.INFO, "Room created"));
        return roomBean;
    }
}