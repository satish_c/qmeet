package org.qmeet.filter;

import org.qmeet.bean.QmeetProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FirstTimeCheckFilter implements Filter {
    public static final String REDIRECT_LOCATION = "/firsttime.html";
    private QmeetProperties qmeetProperties;

    @Override
    public void init(FilterConfig config) throws ServletException {
        ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        qmeetProperties = ctx.getBean(QmeetProperties.class);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        if(qmeetProperties.isFirstTime()) {
            HttpServletRequest request = (HttpServletRequest) req;
            String requestURI = request.getRequestURI();
            if (excludeUri(requestURI)) {
                chain.doFilter(req, resp);
                return;
            }
            HttpServletResponse servletResponse = (HttpServletResponse) resp;
            servletResponse.sendRedirect(REDIRECT_LOCATION);
            return;
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {}

    private boolean excludeUri(String uri) {
        if(qmeetProperties.isStaticResource(uri)) {
            return true;
        }
        if(uri.contains("firsttime")) {
            return true;
        }
        return false;
    }
}