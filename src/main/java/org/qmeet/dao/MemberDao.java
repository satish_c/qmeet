package org.qmeet.dao;

import org.qmeet.model.Member;
import org.qmeet.model.MemberRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface MemberDao extends CrudRepository<Member, Long>{
    public Member findByUserName(String userName);

    public List<Member> findByMemberRole(MemberRole memberRole);

    @Override
    Member save(Member member);

    @Transactional
    public void deleteByUserName(String userName);
}
