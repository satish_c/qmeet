package org.qmeet.dao;

import org.qmeet.model.Config;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigDao extends CrudRepository<Config, Long> {
    public Config findByKey(String key);

    @Override
    Config save(Config config);
}
