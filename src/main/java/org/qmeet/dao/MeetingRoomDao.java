package org.qmeet.dao;

import org.qmeet.model.MeetingRoom;
import org.qmeet.model.Member;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Component
public class MeetingRoomDao  {
    @PersistenceContext(unitName = "h2EntityManagerFactory")
    private EntityManager entityManager;

    public Iterable<MeetingRoom> findAll() {
        Query query = entityManager.createQuery("select m from MeetingRoom m");
        return query.getResultList();
    }

    public MeetingRoom findByRoomId(String roomId) {
        TypedQuery<MeetingRoom> query = entityManager.createQuery("select m from MeetingRoom m where m.url = :url", MeetingRoom.class);
        query.setParameter("url", roomId);
        return query.getSingleResult();
    }

    @Transactional
    public MeetingRoom save(MeetingRoom entity) {
        entityManager.persist(entity);
        return entity;
    }
}