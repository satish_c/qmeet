package org.qmeet.security;

import org.qmeet.dao.MemberDao;
import org.qmeet.model.Member;
import org.qmeet.model.MemberRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ModeratorUserDetailsService implements UserDetailsService {
    @Autowired
    private MemberDao memberDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = memberDao.findByUserName(username);
        if(member != null) {
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority("ROLE_MODERATOR"));
            if(member.getMemberRole().equals(MemberRole.ADMIN)) {
                grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            }
            UserDetails user = new User(member.getUserName(), member.getPasswordHash(), true, true, true, true, grantedAuths);
            return user;
        }
        throw new UsernameNotFoundException(username + " not found");
    }
}
