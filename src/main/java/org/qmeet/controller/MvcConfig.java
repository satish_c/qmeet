package org.qmeet.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/index.html").setViewName("index");
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/logout.html").setViewName("index");
        registry.addViewController("/moderator/dashboard.html").setViewName("moderator");
        registry.addViewController("/moderator/create-meeting-room.html").setViewName("moderator");
        registry.addViewController("/moderator/create-mod.html").setViewName("moderator");
        registry.addViewController("/moderator/home.html").setViewName("moderator");
        registry.addViewController("/moderator/mod-list.html").setViewName("moderator");
        registry.addViewController("/room.html").setViewName("room");
    }
}
