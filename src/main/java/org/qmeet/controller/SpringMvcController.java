package org.qmeet.controller;

import org.qmeet.bean.QmeetProperties;
import org.qmeet.bean.UserBean;
import org.qmeet.rest.bean.LoginBean;
import org.qmeet.rest.bean.Message;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SpringMvcController implements ApplicationContextAware {
    private ApplicationContext applicationContext;
    @Autowired
    private QmeetProperties qmeetProperties;

    @RequestMapping("/firsttime.html")
    public String firstTime(Model model) {
        if(qmeetProperties.isFirstTime()) {
            return "firsttime";
        } else {
            return "redirect:login.html";
        }
    }

    @RequestMapping("/index.html")
    public String index(Model model) {
        UserBean userBean = (UserBean) this.applicationContext.getBean("userBean");
        if(userBean.isUserLoggedIn()) {
            return "redirect:moderator/home.html";
        } else {
            return "index";
        }
    }
    @RequestMapping("/login-error.html")
    public String loginError(Model model) {
        LoginBean loginBean = new LoginBean();
//        loginBean.setUsername(username);
//        loginBean.setPassword(password);
        loginBean.addMessage(new Message(Message.ERROR, "Invalid username or password"));
        model.addAttribute("loginError", true);
        model.addAttribute("loginBean", loginBean);
        return "login";
    }

    @RequestMapping("/room.html")
    public String room(@RequestParam final String roomId) {
        return "room";
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
