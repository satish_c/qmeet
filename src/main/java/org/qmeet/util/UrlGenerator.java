package org.qmeet.util;

import java.util.UUID;

public class UrlGenerator {
    public static String getUniqueString() {
        return UUID.randomUUID().toString();
    }
}
