package org.qmeet.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
@Scope("session")
public class UserBean {

    public boolean isModerator() {
        List<String> roles = getRoles();
        return roles.contains("ROLE_ADMIN") | roles.contains("ROLE_MODERATOR");
    }

    public boolean isAdmin() {
        List<String> roles = getRoles();
        return roles.contains("ROLE_ADMIN");
    }

    private List<String> getRoles() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<String> roles = new ArrayList<>();
        if(principal instanceof User) {
            User user = (User) principal;
            Collection<GrantedAuthority> authorities = user.getAuthorities();
            for(GrantedAuthority authority : authorities) {
                roles.add(authority.getAuthority());
            }
        }
        return roles;
    }

    public boolean isUserLoggedIn() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof User) {
            return true;
        }
        return false;
    }

    public String getUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof User) {
            return ((User) principal).getUsername();
        }
        return "";
    }
}
