package org.qmeet.bean;

import org.qmeet.dao.ConfigDao;
import org.qmeet.model.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class QmeetProperties {
    @Autowired
    private ConfigDao configDao;

    private boolean firstTime;

    // Look for debugMode property. The properties files to look in are defined in applicationContext.xml
    @Value("${qmeet.devMode}" )
    private boolean devMode;

    @PostConstruct
    public void init() {
        // Check if this is the first time the application is running
        Config config = configDao.findByKey(ConfigKeys.FIRST_TIME.getStringValue());
        if(config == null) {
            firstTime = true;
        }
    }

    public boolean isFirstTime() {
        return firstTime;
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }

    public boolean isStaticResource(String uri) {
        if(uri.startsWith("/js/")) {
            return true;
        }
        if(uri.startsWith("/css/")) {
            return true;
        }
        if(uri.startsWith("/images/")) {
            return true;
        }
        return false;
    }

    public String getJavaScriptRootDir() {
        if(devMode) {
            return "/js/src/";
        } else {
            return "/js/build/";
        }
    }
    public boolean isDevMode() {
        return devMode;
    }

    public void setDevMode(boolean devMode) {
        this.devMode = devMode;
    }
}
