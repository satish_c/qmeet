package org.qmeet.bean;

public enum ConfigKeys {
    FIRST_TIME("FIRST_TIME");
    private final String stringValue;

    private ConfigKeys(String value) {
        stringValue = value;
    }

    public String getStringValue() {
        return stringValue;
    }
}
