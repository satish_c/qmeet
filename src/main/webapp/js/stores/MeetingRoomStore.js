"use strict";

var QmeetAjax = require('../util/QmeetAjax.js');
var Rx = require("rx-lite");

var ActionConstants = require("../constants/ActionConstants.js");
var RESOURCE_URL = AppStore.contextRoot + "ws/moderator/meeting-room/";

var MeetingRoomStore = {};
MeetingRoomStore.updates = new Rx.Subject();

function sendUpdate(type, data, messages) {
    var localMessages = [];
    if($.isArray(messages)) {
        localMessages = messages;
    }
    MeetingRoomStore.updates.onNext({type: type, data: data, messages: localMessages});
}

function load() {
    QmeetAjax.get(RESOURCE_URL, function(response) {
        var rooms = response.map(function(room) {
            room.url = AppStore.contextRoot + "room.html?roomId=" + room.url;
            return room;
        });
        sendUpdate(ActionConstants.SUCCESS, rooms, response.messages);
    }, function(messages) {
        sendUpdate(ActionConstants.ERROR, null, messages);
    });
}

function create(room) {
    QmeetAjax.post(RESOURCE_URL, JSON.stringify(room),
        function(responseData) {
            sendUpdate(ActionConstants.CREATE_SUCCESS, responseData, responseData.messages);
        },
        function(messages) {
            sendUpdate(ActionConstants.ERROR, null, messages);
        }
    );
}

function del(roomId) {
    QmeetAjax.del(RESOURCE_URL + roomId, null,
        function(data) {
            sendUpdate(ActionConstants.SUCCESS, null, data.messages);
            load();
        },
        function(messages) {
            sendUpdate(ActionConstants.ERROR, null, messages);
        }
    );
}

function deleteSelected (selectedRoomIds) {
    QmeetAjax.del(RESOURCE_URL + "rooms", JSON.stringify(selectedRoomIds),
        function(data) {
            sendUpdate(ActionConstants.SUCCESS, null, data.messages);
            load();
        },
        function(messages) {
            sendUpdate(ActionConstants.ERROR, null, messages);
        }
    );
}

MeetingRoomStore.observe = function(intent) {
    intent.actions.subscribe(function(actionEvent) {
        switch(actionEvent.action) {
            case ActionConstants.LOAD:
                load();
                break;
            case ActionConstants.CREATE:
                create(actionEvent.data);
                break;
            case ActionConstants.DELETE:
                del(actionEvent.data);
                break;
            case ActionConstants.DELETE_SELECTED:
                deleteSelected(actionEvent.data);
                break;
            default:
            // no op
        }
    });
};

module.exports = MeetingRoomStore;