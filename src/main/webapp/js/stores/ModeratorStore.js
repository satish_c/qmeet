"use strict";

var QmeetAjax = require('../util/QmeetAjax.js');
var Rx = require("rx-lite");

var ActionConstants = require("../constants/ActionConstants.js");
var RESOURCE_URL = AppStore.contextRoot + "ws/moderator/";
var ADMIN_RESOURCE_URL = AppStore.contextRoot + "ws/firsttime/";

var ModeratorStore = {};
ModeratorStore.updates = new Rx.Subject();

function sendUpdate(type, data, messages) {
    var localMessages = [];
    if($.isArray(messages)) {
        localMessages = messages;
    }
    ModeratorStore.updates.onNext({type: type, data: data, messages: localMessages});
}

function load() {
    QmeetAjax.get(RESOURCE_URL, function(response) {
        sendUpdate(ActionConstants.SUCCESS, response, response.messages);
    }, function(messages) {
        sendUpdate(ActionConstants.ERROR, null, messages);
    });
}

function create(user, type) {
    var url = RESOURCE_URL;
    if(type === "admin") {
        url = ADMIN_RESOURCE_URL;
    }
    QmeetAjax.post(url, JSON.stringify(user),
        function(responseData) {
            var type;

            if(responseData.messages.length === 0) {
                type =  ActionConstants.CREATE_SUCCESS;
            } else {
                type =  ActionConstants.ERROR;
            }

            sendUpdate(type, responseData, responseData.messages);
        },
        function(messages) {
            sendUpdate(ActionConstants.ERROR, null, messages);
        }
    );
}

function del(username) {
    QmeetAjax.del(RESOURCE_URL + username, null,
        function(data) {
            sendUpdate(ActionConstants.SUCCESS, null, data.messages);
            load();
        },
        function(messages) {
            sendUpdate(ActionConstants.ERROR, null, messages);
        }
    );
}

function deleteSelected (selectedMods) {
    QmeetAjax.del(RESOURCE_URL + "moderators", JSON.stringify(selectedMods),
        function(data) {
            sendUpdate(ActionConstants.SUCCESS, null, data.messages);
            load();
        },
        function(messages) {
            sendUpdate(ActionConstants.ERROR, null, messages);
        }
    );
}

ModeratorStore.observe = function(intent) {
    intent.actions.subscribe(function(actionEvent) {
        switch(actionEvent.action) {
            case ActionConstants.LOAD:
                load();
                break;
            case ActionConstants.CREATE:
                create(actionEvent.data);
                break;
            case ActionConstants.CREATE_ADMIN:
                create(actionEvent.data, "admin");
                break;
            case ActionConstants.DELETE:
                del(actionEvent.data);
                break;
            case ActionConstants.DELETE_SELECTED:
                deleteSelected(actionEvent.data);
                break;
            default:
            // no op
        }
    });
};

module.exports = ModeratorStore;