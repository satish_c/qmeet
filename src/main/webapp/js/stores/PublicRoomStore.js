"use strict";

var QmeetAjax = require('../util/QmeetAjax.js');
var Rx = require("rx-lite");

var ActionConstants = require("../constants/ActionConstants.js");
var RESOURCE_URL = AppStore.contextRoot + "ws/meeting-room/";

function RoomMessage(userName, txt) {
    this.userName = userName;
    this.txt = txt;
}

function User(name) {
    this.name = name;
}

function getMockRoomMessages() {
    var msgs = [];
    msgs.push(new RoomMessage("satish", "hello"));
    msgs.push(new RoomMessage("chandra", "this is a sentence"));
    msgs.push(new RoomMessage("bakku", "this is a long sentence that goes on and on forevever and still keeps going on and repeats on and on forever..."));
    return msgs;
}

function getMockUsers() {
    var users = [];
    users.push(new User("satish"));
    users.push(new User("chandra"));
    users.push(new User("bakku"));
    return users;
}

function PublicRoomStore(roomId) {
    this.roomId = roomId;
    this.roomMessages = [];
    this.users = [];

    this.updates = new Rx.Subject();

    this.sendUpdate = function (type, data, messages) {
        var localMessages = [];
        if ($.isArray(messages)) {
            localMessages = messages;
        }
        data.roomMessages = getMockRoomMessages();
        data.users = getMockUsers();
        this.updates.onNext({type: type, data: data, messages: localMessages});
    };

    this.load = function () {
        QmeetAjax.get(RESOURCE_URL + this.roomId, function (response) {
            if(UserStore.userLoggedIn === true) {
                response.loggedIn = true;
                response.username = UserStore.username;
            }
            this.sendUpdate(ActionConstants.SUCCESS, response, response.messages);
        }.bind(this), function (messages) {
            this.sendUpdate(ActionConstants.ERROR, null, messages);
        }.bind(this));
    };

    this.observe = function (intent) {
        intent.actions.subscribe(function (actionEvent) {
            switch (actionEvent.action) {
                case ActionConstants.LOAD:
                    this.load();
                    break;
                case ActionConstants.CREATE:
                    this.create(actionEvent.data);
                    break;
                case ActionConstants.DELETE:
                    this.del(actionEvent.data);
                    break;
                case ActionConstants.DELETE_SELECTED:
                    this.deleteSelected(actionEvent.data);
                    break;
                default:
                // no op
            }
        }.bind(this));
    };
}

module.exports = PublicRoomStore;