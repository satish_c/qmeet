"use strict";

var Rx = require("rx-lite");

var PublicRoomIntent = {};

PublicRoomIntent.actions = new Rx.Subject();

PublicRoomIntent.observe = function(component) {
    component.getActions().subscribe(function(actionEvent) {
        PublicRoomIntent.actions.onNext(actionEvent);
    });
};

module.exports = PublicRoomIntent;