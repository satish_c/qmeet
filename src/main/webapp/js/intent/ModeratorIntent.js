"use strict";

var Rx = require("rx-lite");

var ModeratorIntent = {};

ModeratorIntent.actions = new Rx.Subject();

ModeratorIntent.observe = function(component) {
    component.getActions().subscribe(function(actionEvent) {
        ModeratorIntent.actions.onNext(actionEvent);
    });
};

module.exports = ModeratorIntent;