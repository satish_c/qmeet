"use strict";

var Rx = require("rx-lite");

var MeetingRoomIntent = {};

MeetingRoomIntent.actions = new Rx.Subject();

MeetingRoomIntent.observe = function(component) {
    component.getActions().subscribe(function(actionEvent) {
        MeetingRoomIntent.actions.onNext(actionEvent);
    });
};

module.exports = MeetingRoomIntent;