"use strict";

var React = require('react');
var ReactDOM = require('react-dom');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var Rx = require("rx-lite");

var Qmeet = require('./common.jsx');
var ActionConstants = require("../constants/ActionConstants.js");
var ActionEvent = require('../util/ActionEvent.js');

var actions = new Rx.Subject();

module.exports = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    getInitialState: function () {
        return {};
    },
    statics: {
        getActions: function() {
            return actions;
        }
    },
    meetingRoomUpdates : null,
    componentDidMount: function() {
        var meetingRoomStore = this.props.meetingRoomStore;
        this.meetingRoomUpdates = meetingRoomStore.updates.subscribe(function(update) {
            if(update.messages.length > 0) {
                this.addMessages(update.messages);
            }
        }.bind(this));
        ReactDOM.findDOMNode(this.refs.roomName).focus();
    },
    componentWillUnmount: function() {
        this.meetingRoomUpdates.dispose();
    },
    createRoom: function() {
        var data = {name: ReactDOM.findDOMNode(this.refs.roomName).value, durationInMin: ReactDOM.findDOMNode(this.refs.duration).value};
        actions.onNext(new ActionEvent(ActionConstants.CREATE, data));
    },
    render: function () {
        return(
            <div>
                <h2>Create a meeting room</h2>
                <Qmeet.MessagesBox id="messages" messages={this.state.messages}/>
                <form>
                    <div className="row form-group">
                        <div className="col-xs-4">
                            <label htmlFor="roomName">Room Name</label>
                            <Qmeet.Input type="text" id="roomName" ref="roomName" name="roomName"/>
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-xs-4">
                            <label htmlFor="duration">Meeting Duration</label>
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-xs-4">
                            <select id="duration" ref="duration">
                                <option value="15">15 min</option>
                                <option value="30">30 min</option>
                                <option value="60">1 hour</option>
                                <option value="90">1.5 hour</option>
                                <option value="120">2 hours</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group">
                        <Qmeet.Button type="button" name="create" value="Create" onClick={this.createRoom}/>
                    </div>
                </form>
            </div>
        );
    }
});