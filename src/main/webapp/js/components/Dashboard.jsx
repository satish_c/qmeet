"use strict";

var React = require('react');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var Rx = require("rx-lite");

var Qmeet = require('./common.jsx');
var ActionConstants = require("../constants/ActionConstants.js");
var ActionEvent = require('../util/ActionEvent.js');

var actions = new Rx.Subject();

module.exports = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    getInitialState: function () {
        return {};
    },
    statics: {
        getActions: function() {
            return actions;
        }
    },
    meetingRoomUpdates : null,
    componentDidMount: function() {
        var meetingRoomStore = this.props.meetingRoomStore;
        actions.onNext(new ActionEvent(ActionConstants.LOAD, null));
        this.meetingRoomUpdates = meetingRoomStore.updates.subscribe(function(update) {
            if(update.data) {
                this.setState({rooms: update.data});
            }
            if(update.messages.length > 0) {
                this.addMessages(update.messages);
            }
        }.bind(this));
    },
    componentWillUnmount: function() {
        this.meetingRoomUpdates.dispose();
    },
    render: function () {
        var rooms = [];
        if(this.state.rooms) {
            this.state.rooms.forEach(function (room, i) {
                rooms.push(
                    <tr key={i}>
                        <td><a href={room.url}>{room.name}</a></td>
                        <td>{room.expiresIn}</td>
                        <td>{room.memberCount}</td>
                    </tr>
                );
            }.bind(this))
        }
        return (
            <div>
                <h2>Dashboard</h2>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Active Meeting Rooms</h3>
                    </div>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Room</th>
                                <th>Expires in</th>
                                <th>Members</th>
                            </tr>
                        </thead>
                        <tbody>
                        {rooms}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
});