"use strict";

var React = require('react');
var Rx = require("rx-lite");
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var Qmeet = require('./common.jsx');

var ActionConstants = require("../constants/ActionConstants.js");
var ActionEvent = require('../util/ActionEvent.js');

var actions = new Rx.Subject();

module.exports = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    getInitialState: function() {
        return {};
    },
    statics: {
      getActions: function() {
          return actions;
      }
    },
    moderatorUpdates : null,
    componentDidMount: function() {
        var moderatorStore = this.props.moderatorStore;
        actions.onNext(new ActionEvent(ActionConstants.LOAD, null));
        this.moderatorUpdates = moderatorStore.updates.subscribe(function(update) {
            if(update.data) {
                this.setState({moderators: update.data});
            }
            if(update.messages.length > 0) {
                this.addMessages(update.messages);
            }
        }.bind(this));
    },
    componentWillUnmount: function() {
        this.moderatorUpdates.dispose();
    },
    selectAll: function() {
        $("input[name=mod-select-each]").prop('checked', $("input[name=mod-select-all]").prop("checked"));
    },
    deleteSelected: function(e) {
        this.clearMessages();
        var selectedMods = [];
        e.preventDefault();
        $("input[name=mod-select-each]").each(function() {
            if($(this).prop("checked")) {
                selectedMods.push(this.value);
            }
        });
        if(selectedMods.length === 0) {
            this.addWarning("No moderators selected");
            return;
        }
        actions.onNext(new ActionEvent(ActionConstants.DELETE_SELECTED, selectedMods));
        $("input[name=mod-select-all]").prop("checked", false);
    },
    deleteMod: function(username, e) {
        this.clearMessages();
        e.preventDefault();

        actions.onNext(new ActionEvent(ActionConstants.DELETE, username));
    },
    render: function() {
        if(!this.props.isAdmin) {
            return (
                <div></div>
            );
        }

        var members = [];
        if(this.state.moderators) {
            this.state.moderators.forEach(function (moderator, i) {
                members.push(
                    <tr key={i}>
                        <td><input type="checkbox" name="mod-select-each" value={moderator.username}/></td>
                        <td>{moderator.username}</td>
                        <td><input type="button" className="btn btn-default btn-sm" value="Delete" onClick={this.deleteMod.bind(null, moderator.username)}/></td>
                    </tr>
                );
            }.bind(this))
        }
        return(
            <div>
                <Qmeet.MessagesBox id="messages" messages={this.state.messages}/>
                <div className="panel panel-default">
                    <div className="panel-heading">Moderators</div>
                    <div className="panel-body">
                        <p>Bulk actions: <input type="button" className="btn btn-default btn-sm" value="Delete Selected" onClick={this.deleteSelected}/></p>
                    </div>

                    <table className="table">
                        <thead>
                        <tr>
                            <th className="col-md-1"><input type="checkbox" name="mod-select-all" onChange={this.selectAll}/></th>
                            <th className="col-md-3">User name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {members}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
});