"use strict";
var React = require('react');
var ReactDOM = require('react-dom');
var Rx = require("rx-lite");

var Qmeet = require('./common.jsx');
var ModeratorStore = require('../stores/ModeratorStore.js');
var ModeratorIntent = require('../intent/ModeratorIntent.js');


var actions = new Rx.Subject();

var FirstTimePage = React.createClass({
    statics: {
        getActions: function() {
            return actions;
        }
    },
    render: function() {
        return(
            <div>
                <h1>qMeet Installation</h1>
                <p className="lead">Before you can run qMeet, you need to create an admin user. Enter details below:</p>
                <Qmeet.RegisterForm actions={actions} updates={this.props.moderatorStore.updates} userType="admin"/>
            </div>
        );
    }
});

ModeratorIntent.observe(FirstTimePage);
ModeratorStore.observe(ModeratorIntent);

ReactDOM.render(
    <FirstTimePage moderatorStore={ModeratorStore}/>, document.getElementById('content')
);