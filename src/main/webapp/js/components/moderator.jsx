"use strict";

var React = require('react');
var Rx = require("rx-lite");

var Qmeet = require('./common.jsx');
var QmeetController = require('./QmeetController.jsx');
var viewController = QmeetController.ViewController;

var ModeratorOptions = require('./ModeratorOptions.jsx');
var ModeratorList = require('./ModeratorList.jsx');
var CreateModerator = require('./CreateModerator.jsx');
var CreateRoom = require('./CreateRoom.jsx');
var Dashboard = require('./Dashboard.jsx');

var ModeratorStore = require('../stores/ModeratorStore.js');
var ModeratorIntent = require('../intent/ModeratorIntent.js');

var MeetingRoomStore = require('../stores/MeetingRoomStore.js');
var MeetingRoomIntent = require('../intent/MeetingRoomIntent.js');

var ModeratorPage = React.createClass({
   render: function() {
       return(
           <div>
               <Qmeet.Header/>
               <div className="row">
                   <div className="col-md-3">
                       <ModeratorOptions isAdmin={UserStore.admin}/>
                   </div>
                   <div className="col-md-9">
                       {this.props.contentPanel}
                   </div>
               </div>
           </div>
       );
   }
});

var viewComponentMap = {};
viewComponentMap[QmeetController.MODERATOR_HOME] = <ModeratorPage contentPanel={<Dashboard meetingRoomStore={MeetingRoomStore}/>}/>;
viewComponentMap[QmeetController.CREATE_ROOM] = <ModeratorPage contentPanel={<CreateRoom meetingRoomStore={MeetingRoomStore}/>}/>;
MeetingRoomIntent.observe(CreateRoom);
MeetingRoomIntent.observe(Dashboard);
MeetingRoomStore.observe(MeetingRoomIntent);

viewComponentMap[QmeetController.MODERATOR_LIST] = <ModeratorPage contentPanel={<ModeratorList isAdmin={UserStore.admin} moderatorStore={ModeratorStore} />}/>;
ModeratorIntent.observe(ModeratorList);
ModeratorStore.observe(ModeratorIntent);

viewComponentMap[QmeetController.CREATE_MODERATOR] = <ModeratorPage contentPanel={
    <CreateModerator
        view={viewController.getView(QmeetController.CREATE_MODERATOR)}
        successView={QmeetController.MODERATOR_LIST}
        moderatorStore={ModeratorStore}/>
} />;
ModeratorIntent.observe(CreateModerator);

viewController.init(viewComponentMap, AppStore.contextRoot, document.getElementById('content'));
