"use strict";

var React = require('react');
var ReactDOM = require('react-dom');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var QmeetController = require('./QmeetController.jsx');
var viewController = QmeetController.ViewController;
var ActionConstants = require("../constants/ActionConstants.js");
var ActionEvent = require('../util/ActionEvent.js');

var Qmeet = {};

Qmeet.MessagesMixin = {
    componentWillMount: function() {
        this.messages = [];
        this.setState({messages: this.messages});
    },
    addWarning: function(msgText) {
        this.messages.push({type: "warning", text: msgText});
        this.setState({messages: this.messages});
    },
    addInfo: function(msgText) {
        this.messages.push({type: "info", text: msgText});
        this.setState({messages: this.messages});
    },
    addError: function(msgText) {
        this.messages.push({type: "danger", text: msgText});
        this.setState({messages: this.messages});
    },
    addMessage: function(message) {
        this.messages.push(message);
        this.setState({messages: this.messages});
    },
    addMessages: function(messages) {
        this.messages = messages;
        this.setState({messages: this.messages});
    },
    clearMessages: function() {
        while(this.messages.length > 0) {
            this.messages.pop();
        }
        this.setState({messages: this.messages});
    }
};

Qmeet.Input = React.createClass({
    render: function () {
        return (
            <input type="text" className="form-control" {...this.props} />
        );
    }
});

Qmeet.Anchor = React.createClass({
    render: function () {
        return (
            <a {...this.props}>{this.props.children}</a>
        );
    }
});

Qmeet.Button = React.createClass({
    render: function () {
        return (
            <input className="btn btn-default" {...this.props}/>
        );
    }
});

Qmeet.CsrfData = React.createClass({
   render: function() {
       return(
           <input type="hidden" name={AppStore.csrfParam} value={AppStore.csrfToken} {...this.props}/>
       );
   }
});

Qmeet.MessagesBox = React.createClass({
    render: function() {
        var messages = [];
        if(this.props.messages) {
            this.props.messages.forEach(function (message, index) {
                messages.push(<div key={index} className={"alert alert-" + message.type}>{message.text}</div>)
            });
        }
        return (
            <div id={this.props.id}>
                {messages}
            </div>
        );
    }
});

Qmeet.RegisterForm = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    getInitialState: function() {
        return {username: "", password: ""};
    },
    storeUpdates : null,
    componentDidMount: function() {
        ReactDOM.findDOMNode(this.refs.username).focus();
        this.storeUpdates = this.props.updates.subscribe(function(update) {
            if(update.type === ActionConstants.CREATE_SUCCESS) {
                if(this.props.successView) {
                    viewController.goToView(this.props.successView, document.getElementById('content'));
                } else {
                    window.location = "/";
                }
            }
            if(update.messages.length > 0) {
                this.addMessages(update.messages);
            }
        }.bind(this));
    },
    componentWillUnmount: function() {
        this.storeUpdates.dispose();
    },
    handleSubmit: function(e) {
        e.preventDefault();
        this.clearMessages();
        if (this.state.username === "" || this.state.password === "") {
            if(this.state.username === "") {
                this.addWarning("Username is required");
            }
            if(this.state.password === "") {
                this.addWarning("Password is required");
            }
            return;
        }
        if(this.props.userType === "admin") {
            this.props.actions.onNext(new ActionEvent(ActionConstants.CREATE_ADMIN, this.state));
        } else {
            this.props.actions.onNext(new ActionEvent(ActionConstants.CREATE, this.state));
        }
    },
    render: function() {
        return(
            <div className="row">
                <div className="col-md-4">
                    <Qmeet.MessagesBox id="messages" messages={this.state.messages}/>
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="username">User name:</label>
                            <Qmeet.Input type="text" maxLength="128" valueLink={this.linkState('username')} ref="username" id="username"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password:</label>
                            <Qmeet.Input type="password" maxLength="128" valueLink={this.linkState('password')} id="password"/>
                        </div>
                        <div className="form-group">
                            <Qmeet.Button type="submit" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
});

Qmeet.LogoutForm = React.createClass({
    logout: function(e) {
        e.preventDefault();
        ReactDOM.findDOMNode(this.refs.logoutForm).submit();
    },
    render: function () {
        return (
            <li className="text-right">
                <Qmeet.Anchor href="#" onClick={this.logout}>Logout</Qmeet.Anchor>
                <form action="/logout" method="post" ref="logoutForm">
                    <Qmeet.CsrfData />
                </form>
            </li>
        )
    }
});

Qmeet.Header = React.createClass({
    render: function () {
        var components = [];
        if(UserStore.userLoggedIn === true) {
            if(UserStore.admin === true || UserStore.moderator === true) {
                var view = viewController.getView(QmeetController.MODERATOR_HOME);
                components.push(<li className="text-right" key="mod"><Qmeet.Anchor href={view.url}>Moderator</Qmeet.Anchor></li>);
            }
            components.push(<Qmeet.LogoutForm key="logout"/>);
        } else {
            components.push(<li className="text-right" key="login"><Qmeet.Anchor href="/login.html">Login</Qmeet.Anchor></li>);
        }

        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    {/* Brand and toggle get grouped for better mobile display */}
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                        </button>
                        <a className="navbar-brand" href="/">qMeet</a>
                    </div>

                    {/* Collect the nav links, forms, and other content for toggling */}
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav navbar-right">
                            {components}
                        </ul>
                    </div>{/* /.navbar-collapse */}
                </div>{/*/.container-fluid */}
            </nav>
        );
    }
});

module.exports = Qmeet;