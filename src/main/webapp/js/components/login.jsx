var React = require('react');
var ReactDOM = require('react-dom');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var Qmeet = require('./common.jsx');
var QmeetController = require('./QmeetController.jsx');
var viewController = QmeetController.ViewController;

var LoginPage = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    getInitialState: function() {
        return {username: "", password: ""};
    },
    componentDidMount: function() {
        this.addMessages(this.props.messages);
        ReactDOM.findDOMNode(this.refs.username).focus();
    },
    render: function() {
        return(
            <div>
                <Qmeet.Header/>
                <h1>Login</h1>
                <p className="lead">To create a meeting room, you need to login. To get a login, contact the administrator.</p>
                <div className="row">
                    <div className="col-md-4">
                        <Qmeet.MessagesBox id="messages" messages={this.state.messages}/>
                        <form action={this.props.url} method="post">
                            <Qmeet.CsrfData />
                            <div className="form-group">
                                <label htmlFor="username">User name:</label>
                                <Qmeet.Input type="text" name="username" maxLength="128" valueLink={this.linkState('username')} ref="username" id="username"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password:</label>
                                <Qmeet.Input type="password" name="password" maxLength="128" valueLink={this.linkState('password')} id="password"/>
                            </div>
                            <div className="form-group">
                                <input type="checkbox" name="remember-me" id="remember-me"/> <label htmlFor="remember-me">Remember me</label>
                            </div>
                            <div className="form-group">
                                <Qmeet.Button type="submit" value="Submit"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
});

var viewComponentMap = {};
viewComponentMap[QmeetController.LOGIN] = <LoginPage url="/login" messages={messages}/>;
viewController.init(viewComponentMap, AppStore.contextRoot, document.getElementById('content'));