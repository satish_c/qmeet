"use strict";

var React = require('react');
var Rx = require("rx-lite");
var LinkedStateMixin = require('react-addons-linked-state-mixin');

var Qmeet = require('./common.jsx');
var QmeetController = require('./QmeetController.jsx');
var viewController = QmeetController.ViewController;
var PublicRoomStore = require('../stores/PublicRoomStore.js');
var PublicRoomIntent = require('../intent/PublicRoomIntent.js');

var ActionConstants = require("../constants/ActionConstants.js");
var ActionEvent = require('../util/ActionEvent.js');

var actions = new Rx.Subject();

var RoomMessages = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    render: function() {
        var list = [];
        if(this.props.roomMessages) {
            this.props.roomMessages.forEach(function (roomMessage) {
                list.push(<li><strong>{roomMessage.userName}</strong> : {roomMessage.txt}</li>);
            });
        }
        return(
            <div id="roomMessages" className="col-md-12">
                <ul>
                    {list}
                </ul>
            </div>
        );
    }
});
var UserList = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    render: function() {
        var list = [];
        if(this.props.users) {
            this.props.users.forEach(function (user) {
                list.push(<li><strong>{user.name}</strong></li>);
            });
        }
        return(
            <div id="userList" className="col-md-12">
                <ul>
                    {list}
                </ul>
            </div>
        );
    }
});

var RoomPage = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    getInitialState: function() {
        return {
            room: {
                loggedIn: false
            },
            username: ""
        };
    },
    statics: {
        getActions: function() {
            return actions;
        }
    },
    roomUpdates : null,
    componentDidMount: function() {
        var roomStore = this.props.roomStore;
        actions.onNext(new ActionEvent(ActionConstants.LOAD, null));
        this.roomUpdates = roomStore.updates.subscribe(function(update) {
            if(update.data) {
                this.setState({room: update.data});
            }
            if(update.messages.length > 0) {
                this.addMessages(update.messages);
            }
        }.bind(this));
    },
    componentWillUnmount: function() {
        this.roomUpdates.dispose();
    },
    render: function() {
        var loggedIn = "";
        if(this.state.room.loggedIn === true) {
            loggedIn = <p>Logged in as {this.state.room.username}</p>
        } else {
            loggedIn =
                <div className="row">
                    <div className="col-md-4">
                        <div className="form-group">
                            <label htmlFor="username">Chose an user name:</label>
                            <Qmeet.Input type="text" maxLength="128" valueLink={this.linkState('username')} ref="username" id="username"/>
                        </div>
                        <div className="form-group">
                            <Qmeet.Button type="button" value="Submit"/>
                        </div>
                    </div>
                </div>
        }
        return(
            <div>
                <Qmeet.Header/>
                {loggedIn}
                <Qmeet.MessagesBox id="messages" messages={this.state.messages}/>
                <h2>Room: {this.state.room.name}</h2>
                <div className="row">
                    <div className="col-md-9">
                        <RoomMessages roomMessages={this.state.room.roomMessages}/>
                    </div>
                    <div className="col-md-3">
                        <UserList users={this.state.room.users}/>
                    </div>
                </div>
                <div className="row top-buffer">
                    <div className="col-md-9">
                        <form action="">
                            <textarea className="form-control" rows="7"></textarea>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
});

function getParam(name){
    if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search)) {
        return decodeURIComponent(name[1]);
    }
}

var store = new PublicRoomStore(getParam("roomId"));
store.observe(PublicRoomIntent);
PublicRoomIntent.observe(RoomPage);

var viewComponentMap = {};
viewComponentMap[QmeetController.ROOM] = <RoomPage roomStore={store}/>;

viewController.init(viewComponentMap, AppStore.contextRoot, document.getElementById('content'));