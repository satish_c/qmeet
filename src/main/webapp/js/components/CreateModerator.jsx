"use strict";

var React = require('react');
var Rx = require("rx-lite");

var LinkedStateMixin = require('react-addons-linked-state-mixin');
var Qmeet = require('./common.jsx');

var actions = new Rx.Subject();

module.exports = React.createClass({
    mixins: [LinkedStateMixin, Qmeet.MessagesMixin],
    getInitialState: function () {
        return {};
    },
    statics: {
        getActions: function() {
            return actions;
        }
    },
    componentDidMount: function () {
        this.addMessages(this.props.messages);
    },
    render: function () {
        return(
            <Qmeet.RegisterForm successView={this.props.successView} actions={actions} updates={this.props.moderatorStore.updates}/>
        );
    }
});