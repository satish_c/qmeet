"use strict";
var ReactDOM = require('react-dom');

var QmeetController = {
    APP_HOME:  "appHome",
    APP_INDEX_HOME:  "appIndexHome",
    CREATE_MODERATOR: "createModerator",
    CREATE_ROOM: "createRoom",
    DASHBOARD: "dashboard",
    LOGIN: "login",
    MODERATOR_HOME: "moderatorHome",
    MODERATOR_LIST: "moderatorList",
    ROOM: "room"
};

var qmeetViews  = [
    new View(QmeetController.APP_HOME, "Home", AppStore.contextRoot),
    new View(QmeetController.APP_INDEX_HOME, "Home", AppStore.contextRoot + "index.html"),
    new View(QmeetController.CREATE_MODERATOR, "Create Moderator", AppStore.contextRoot + "moderator/create-mod.html"),
    new View(QmeetController.CREATE_ROOM, "Create Meeting Room", AppStore.contextRoot + "moderator/create-meeting-room.html"),
    new View(QmeetController.DASHBOARD, "Dashboard", AppStore.contextRoot + "moderator/dashboard.html"),
    new View(QmeetController.LOGIN, "Login", AppStore.contextRoot + "login.html"),
    new View(QmeetController.MODERATOR_HOME, "Moderator Home", AppStore.contextRoot + "moderator/home.html"),
    new View(QmeetController.MODERATOR_LIST, "Moderator List", AppStore.contextRoot + "moderator/mod-list.html"),
    new View(QmeetController.ROOM, "Room", AppStore.contextRoot + "room.html")
];

function View(name, title, url) {
    this.name = name;
    this.title = title;
    this.url = url;
    this.search = "";
}

var viewMap = {};
qmeetViews.forEach(function(view) {
    viewMap[view.name] = view;
});

QmeetController.ViewController = (function(views) {
    var viewMap = {};
    var currentView = null;
    var viewComponentMap = {};

    views.forEach(function(view) {
        viewMap[view.name] = view;
    });

    function init(obj, contextRoot, container) {
        viewComponentMap = obj;

        console.log("path:", window.location.pathname);
        var defaultView = null;
        for(var i=0; i<views.length; i++) {
            var view = views[i];
            console.log("view.url:", view.url);
            if(view.url === window.location.pathname) {
                view.search = window.location.search
                defaultView = view;
                break;
            }
        }
        goToView(defaultView.name, container);
    }

    function getView(name) {
        return viewMap[name];
    }

    window.onpopstate = function(event) {
        if(event.state) {
            ReactDOM.unmountComponentAtNode(document.getElementById('content'));
            ReactDOM.render(viewComponentMap[event.state], document.getElementById('content'));
        }
    };

    function goToView(name, container) {
        var view = getView(name);

        if(currentView != null) {
            window.history.pushState(currentView.name, view.title, view.url + view.search);
        }
        window.history.replaceState(view.name, view.title, view.url + view.search);

        currentView = view;
        console.log("Loading view:", view);
        ReactDOM.unmountComponentAtNode(container);
        ReactDOM.render(viewComponentMap[name], container);
    }

    function getCurrentView() {
        return currentView;
    }

    return {
        init: init,
        getView: getView,
        goToView: goToView,
        getCurrentView: getCurrentView
    };
})(qmeetViews);

module.exports = QmeetController;