"use strict";

var React = require('react');
var Qmeet = require('./common.jsx');
var QmeetController = require('./QmeetController.jsx');
var viewController = QmeetController.ViewController;

var HomePage = React.createClass({
    render: function() {
        return(
            <div>
                <Qmeet.Header/>
                <h1>qMeet</h1>
                <p>Create an online meeting room where anyone who you share the meeting url can join in without any registration. These meeting rooms are pseudo-anonymous and are akin to a real world open meet.</p>
                <Qmeet.Anchor href="/moderator/home.html" className="btn btn-default">Login</Qmeet.Anchor>
            </div>
        );
    }
});

var viewComponentMap = {};
viewComponentMap[QmeetController.APP_HOME] = <HomePage/>;
viewComponentMap[QmeetController.APP_INDEX_HOME] = <HomePage/>;

viewController.init(viewComponentMap, AppStore.contextRoot, document.getElementById('content'));