"use strict";

var React = require('react');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var Qmeet = require('./common.jsx');
var QmeetController = require('./QmeetController.jsx');
var viewController = QmeetController.ViewController;

module.exports = React.createClass({
    mixins: [LinkedStateMixin],
    getInitialState: function () {
        var links = [
            {viewName: QmeetController.MODERATOR_HOME, active: false, title: "Dashboard"},
            {viewName: QmeetController.CREATE_ROOM, active: false, title: "Create Meeting Room"},
            {viewName: QmeetController.CREATE_MODERATOR, active: false, title: "Create Moderator", admin: true},
            {viewName: QmeetController.MODERATOR_LIST, active: false, title: "Moderators", admin: true}
        ];
        return {links: links};
    },
    componentDidMount: function () {
        this.setActiveOption(viewController.getCurrentView().name);
    },
    componentWillReceiveProps: function() {
        this.setActiveOption(viewController.getCurrentView().name);
    },
    setActiveOption: function(viewName) {
      var links = this.state.links;
      links.forEach(function(link) {
          if(link.viewName === viewName) {
              link.active = true;
          } else {
              link.active = false;
          }
      });
      this.setState({links: links})
    },
    linkAction: function(viewName, e) {
        e.preventDefault();
        viewController.goToView(viewName, document.getElementById('content'));
    },
    render: function () {
        var options = [];
        this.state.links.forEach(function(link, i) {
            var view = viewController.getView(link.viewName);
            var includeLink = false;
            if(link.admin) {
                if(this.props.isAdmin) {
                    includeLink = true;
                }
            } else {
                includeLink = true;
            }
            if(includeLink) {
                var className = "";
                if(link.active) {
                    className = "active";
                }
                options.push(<li role="presentation" className={className} key={i}>
                    <Qmeet.Anchor href={view.url} onClick={this.linkAction.bind(null, link.viewName)}>{link.title}</Qmeet.Anchor></li>)
            }
        }.bind(this));

        return(
            <div>
                <ul className="nav nav-pills nav-stacked">
                    {options}
                </ul>
            </div>
        );
    }
});