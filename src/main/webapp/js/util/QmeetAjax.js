"use strict";

module.exports = (function() {

    function _jQueryAjax(httpMethod, appUrl, data, successFunction, errorFunction) {
        $.ajax({
            url: appUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: httpMethod,
            data: data,
            beforeSend: function (request) {
                request.setRequestHeader(AppStore.csrfHeader, AppStore.csrfToken);
            },
            success: successFunction,
            error: function (xhr, status, err) {
                var messages;
                if (xhr.status == 400) {
                    var responseData = JSON.parse(xhr.responseText);
                    if (responseData.messages) {
                        messages = responseData.messages;
                    } else {
                        messages = [{type: "danger", text: responseData}];
                    }
                } else {
                    messages = [{type: "danger", text: err.toString()}];
                    console.error(appUrl, status, err.toString());
                }
                if(errorFunction) {
                    errorFunction(messages);
                }
            }
        });
    }

    function post(appUrl, data, successFunction, errorFunction) {
        _jQueryAjax('POST', appUrl, data, successFunction, errorFunction);
    }

    function get(appUrl, successFunction, errorFunction) {
        _jQueryAjax('GET', appUrl, null, successFunction, errorFunction);
    }

    function del(appUrl, data, successFunction, errorFunction) {
        _jQueryAjax('DELETE', appUrl, data, successFunction, errorFunction);
    }

    function synchronousGet(appUrl) {
        return JSON.parse($.ajax({
            type: "GET",
            url: appUrl,
            async: false
        }).responseText);
    }

    return {
        post: post,
        get: get,
        del: del,
        synchronousGet: synchronousGet
    }
})();