"use strict";

var ActionsConstants = {
    CREATE: "create",
    CREATE_ADMIN: "create_admin",
    CREATE_SUCCESS: "create_success",
    DELETE : "delete",
    DELETE_SELECTED : "deleted_selected",
    ERROR: "error",
    LOAD : "load",
    SUCCESS: "success"
};

module.exports = ActionsConstants;